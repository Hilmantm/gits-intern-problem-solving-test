# GITS Indonesia internship test

## Tech Stack
Bahasa pemrograman: **Go**

## Cara Penggunaan
1. Akan disajikan pilihan menu pada aplikasi ini
2. Pilih berdasarkan nomor menu kecuali untuk menu exit
3. Inputkan data sesuai dengan instruksi
4. Inputkan -1 pada input pilihan ketika sudah selesai menggunakan aplikasi ini

## Hasil
### 1. A000124 of Sloane’s OEIS
![Hasil dari A000124 of Sloane’s OEIS Problem](./result/A000124%20of%20Sloane’s%20OEIS%20Problem%20Result.png)
### 2. Dense Ranking
![Hasil dari Dense Ranking Problem](./result/Dense%20Ranking%20Problem%20Result.png)
### 3. Balanced Brackets
![Hasil dari Balanced Brackets Problem](./result/Balanced%20Brackets%20Problem%20Result.png)

## Jawaban Soal No.3
Kompleksitas algoritma adalah O(n) karena melalui setiap karakter dalam input string sebanyak satu kali.
Oleh karena itu, kompleksitas waktu algoritma ini sebanding dengan panjang input string.
Ini adalah algoritma dengan kompleksitas terendah yang bisa saya sajikan.

Balanced Brackets ini sebenarnya mirip dengan permasalahan palindrome (kata yang bisa dibulak-balik). Tentunya jika posisi
kurung berurutan bisa diimplementasikan pada penyelesaian permasalahan palindrome ini. Tetapi karena contoh input terakhir<br>
**input**	: { ( ( [ ] ) [ ] ) [ ] } <br>
**output**	: true (Setiap braket seimbang, antara braket buka dan braket tutup, meskipun struktur braket tidak beraturan) <br>
Maka diterapkan konsep struktur data stack untuk solusi permasalan balanced brackets ini

