package main

// isBalance merupakan fungsi untuk cek apakah kurung pada string seimbang
// Aturannya setiap kurung buka harus memiliki pasangannya yaitu kurung tutup
// Konsep yang diimplementasikan pada fungsi ini adalah konsep struktur data stack
// input	: { [ ( ) ] }
// output	: true (Setiap braket seimbang, antara braket buka dan braket tutup)
// input	: { [ ( ] ) }
// output	: false (String { [ ( ] ) } tidak seimbang untuk karakter yang diapit oleh { dan } yaitu [ ( ] ).)
// input	: { ( ( [ ] ) [ ] ) [ ] }
// output	: true (Setiap braket seimbang, antara braket buka dan braket tutup, meskipun struktur braket tidak beraturan)
// Kompleksitas algoritma adalah O(n) karena melalui setiap karakter dalam input string sebanyak satu kali
// Oleh karena itu, kompleksitas waktu algoritma ini sebanding dengan panjang input string
func isBalance(input string) bool {

	// variable untuk menampung stack
	stack := []rune{}

	// dibuat map untuk mengimplementasikan key value
	// map ini akan digunakan untuk mencocokkan antara karakter dengan top pada stack
	bracketPairs := map[rune]rune{
		'}': '{',
		']': '[',
		')': '(',
	}

	// loop untuk setiap karakter pada string / input
	for _, char := range input {

		// jika karakter merupakan kurung buka, maka masukkan ke dalam stack
		// jika karakter merupakan kurung tutup, maka dikomparasi dengan value yang ada pada bracketPairs
		if char == '{' || char == '[' || char == '(' {
			stack = append(stack, char)
		} else if char == '}' || char == ']' || char == ')' {
			// jika stack sudah kosong padahal masih ada kurung tutup pada input, maka kurung tidak seimbang
			if len(stack) == 0 {
				return false
			}

			// ambil top untuk proses komparasi
			top := stack[len(stack)-1]
			// hapus elemen terakhir pada stack / bahasa familiarnya adalah pop
			stack = stack[:len(stack)-1]

			// komparasikan apakah value (kurung buka) dari kurung tutup sekarang sama dengan top pada stack
			// jika berbeda, maka artinya kurung tidak seimbang
			if bracketPairs[char] != top {
				return false
			}
		}
	}

	return len(stack) == 0
}

func yesOrNo(good bool) string {
	if good {
		return "YES"
	} else {
		return "NO"
	}
}
