package main

import "fmt"

func getSloanesA000124Sequences(n int) []int {
	sequence := make([]int, n)

	for i := 0; i < n; i++ {
		sequence[i] = sloanesA000124Formula(i)
	}

	return sequence
}

func sloanesA000124Formula(n int) int {
	// formula ini didapat dari https://oeis.org/A000124
	return n*(n+1)/2 + 1
}

func getOutputSloanesA000124Sequences(input int) {
	sequence := getSloanesA000124Sequences(input)

	fmt.Print("Output: ")
	for i, num := range sequence {
		if i != len(sequence)-1 {
			fmt.Printf("%d-", num)
		} else {
			fmt.Printf("%d\n", num)
		}
	}
}
