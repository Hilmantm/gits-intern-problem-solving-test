package main

import (
	"fmt"
)

const (
	EXIT                    = -1
	SLOANES_PROBLEM         = 1
	DENSERANKING_PROBLEM    = 2
	BALANCEDBRACKET_PROBLEM = 3
)

func main() {

	var choose int
	fmt.Println("Pilih Soal:\n1. Sloane's Problem\n2. Dense Ranking Problem\n3. Balanced Bracket Problem\n4. Exit (enter -1)")
	fmt.Print("Pilihan: ")
	fmt.Scanln(&choose)

	for choose != EXIT {
		switch choose {
		case SLOANES_PROBLEM:
			var input int
			fmt.Print("Masukkan jumlah anggota dalam deret: ")
			fmt.Scanln(&input)
			getOutputSloanesA000124Sequences(input)
		case DENSERANKING_PROBLEM:
			var playerCount, gitsCount int

			fmt.Print("Masukkan banyaknya pemain: ")
			fmt.Scanln(&playerCount)
			playersScore := make([]int, playerCount)
			fmt.Println("Masukkan Skor: ")
			for i := 0; i < playerCount; i++ {
				fmt.Printf("Skor pemain %d: ", i+1)
				fmt.Scanln(&playersScore[i])
			}

			fmt.Print("Masukkan banyaknya poin GITS: ")
			fmt.Scanln(&gitsCount)
			gitsScore := make([]int, gitsCount)
			fmt.Println("Masukkan Skor GITS: ")
			for i := 0; i < gitsCount; i++ {
				fmt.Printf("Skor pemain %d: ", i+1)
				fmt.Scanln(&gitsScore[i])
			}

			playersPlayResult := calculatePlayerRank(playersScore)
			gitsPlayResult := calculateCurrentPlayerScore(playersPlayResult, gitsScore)
			fmt.Println(gitsPlayResult)
		case BALANCEDBRACKET_PROBLEM:
			var brackets string
			fmt.Print("Input brackets: ")
			fmt.Scanln(&brackets)
			fmt.Println("Kurung seimbang?", yesOrNo(isBalance(brackets)))
		}
		fmt.Print("Pilihan: ")
		fmt.Scanln(&choose)
	}

}
