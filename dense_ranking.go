package main

import (
	"sort"
)

// playResult merupakan objek yang digunakan untuk menyimpan data score dan ranking untuk setiap player dengan score tertinggi sebelumnya
// objek ini digunakan untuk proses komparasi dengan score yang diperoleh oleh peserta sekarang
type playResult struct {
	score int
	rank  int
}

// calculatePlayerRank digunakan untuk menghitung ranking dari setiap player yang memiliki skor tertinggi
// fungsi ini akan menghasilkan array of playResult
func calculatePlayerRank(scores []int) []playResult {

	// variable hasil yang bertipe array of playResult
	var result []playResult

	// currentRanking dibuat untuk tracking ranking dari player-player sebelunya
	currentRanking := 1

	// sorting score dari tertinggi hingga terendah agar penentuan score berurutan
	// dari ranking tertinggi hingga terendah
	copyScores := scores
	sort.Sort(sort.Reverse(sort.IntSlice(copyScores)))

	// looping dari setiap score
	for i := 0; i < len(copyScores); i++ {

		// buatkan objek playResult dan tambahkan ke dalam variable result
		result = append(result, playResult{score: copyScores[i], rank: currentRanking})

		// selama posisi sekarang kurang dari ukuran score dikurang satu
		// maka ranking masih bisa bertambah
		if i < len(copyScores)-1 {
			// jika score posisi sekarang lebih besar dari posisi selanjutnya
			// maka ranking bertambah, jika score sama, maka tidak perlu ditambah rankingnya
			if copyScores[i] > copyScores[i+1] {
				currentRanking++
			}
		}
	}

	return result
}

func calculateCurrentPlayerScore(playerScores []playResult, currentPlayerScores []int) []playResult {
	var result []playResult

	currentPlayerScoresSize := len(currentPlayerScores)
	playerScoresSize := len(playerScores)

	for i := 0; i < currentPlayerScoresSize; i++ {
		for j := 0; j < playerScoresSize-1; j++ {
			var current playResult

			// cek apakah lebih besar sama dengan score dengan ranking 1
			if currentPlayerScores[i] >= playerScores[j].score && playerScores[j].rank == 1 {
				current = playResult{score: currentPlayerScores[i], rank: 1}
				result = append(result, current)
				break
			}

			// cek apakah kurang dari score dengan ranking terakhir, maka ranking harus ditambah satu
			// artinya scorenya lebih jelek dan ranking pasti lebih kecil dari player sebelumnya
			if currentPlayerScores[i] < playerScores[playerScoresSize-1].score {
				current = playResult{score: currentPlayerScores[i], rank: playerScores[playerScoresSize-1].rank + 1}
				result = append(result, current)
				break
			}

			// cek apakah ada diantara rank sebelumnya dan rank selanjutnya
			if currentPlayerScores[i] >= playerScores[j+1].score && currentPlayerScores[i] < playerScores[j].score && playerScores[j+1].score != playerScores[j].score {
				current = playResult{score: currentPlayerScores[i], rank: playerScores[j+1].rank}
				result = append(result, current)
				break
			}
		}
	}

	return result
}
